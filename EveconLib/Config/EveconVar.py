
title_time = None  # title time init later
title_refresh_time = 2.5

myType = ""

# servers init later

globalMPports = None
globalMPportsJava = None

logServer = None
logServerPort = 4222

ddbugger = None

# filePaths

import sys
if sys.platform == "win32":
    path_seg = "\\"
else:
    path_seg = "/"

environmentPath = ""  # standard path

infoPath = environmentPath + "data" + path_seg + "Info" + path_seg
versionFile = infoPath + "version"
changelogFile = infoPath + "Changelog.txt"

notiePath = environmentPath + "data" + path_seg + "Noties"
outputPath = environmentPath + "data" + path_seg + "Output"

tmpPath = environmentPath + "data" + path_seg + "tmp" + path_seg

usedPortsFile = tmpPath + "usedPorts.txt"

dataPath = environmentPath + "data" + path_seg + "Data" + path_seg

nheeDir = dataPath + "nhee" + path_seg
foxiDir = dataPath + "foxi" + path_seg

configPath = environmentPath + "data" + path_seg + "Config" + path_seg

splWeapFile = configPath + "splWeap.json"
MusicFile = configPath + "Music.json"
deacSSFile = configPath + "deacSS"

logFile = environmentPath + "data" + path_seg + "log.txt"

backupPath = environmentPath + "data" + path_seg + "Backup" + path_seg

backupMusicFile = backupPath + "Music.json"

icoPath = environmentPath + "data" + path_seg + "Ico" + path_seg
radioIcoFile = icoPath + "Radio.ico"


