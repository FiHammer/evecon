from EveconLib.Config.StaticVar import *
from EveconLib.Config.EveconVar import *

# startup
import configparser
import os
import socket

del EveconVar
del StaticVar

loadFull = False

# user debug config
suppressErrors = False
alwaysPrintLog = False

# startProgram = os.getcwd() + path_seg + "!Evecon" + path_seg + "dev" + path_seg + "!Console.py"
startProgram = os.path.abspath(sys.argv[0])

if not os.path.exists(startProgram):
    if suppressErrors:
        print("Start programm could not be found! Please import this library in the dir you started this program!")
    else:
        input("Start programm could not be found! Please import this library in the dir you started this program!\nYou can go on (Enter), but something could happen")

file_versions = []
file_version = code_version


# load environment
curPath = os.getcwd()
curPathSplit = curPath.split(path_seg)
if os.path.exists("EveconLib"+path_seg+"Config"+path_seg+"stdenv"):  # variant one (recommended): in EveconLib/Config/stdenv is the std environment
    with open("EveconLib"+path_seg+"Config"+path_seg+"stdenv") as file:
        lines = file.readlines()
        if lines:
            if os.path.exists(lines[0]):
                environmentPath = lines[0].rstrip(path_seg)
elif curPath == "C:\\Users\\Mini-Pc Nutzer.000\\Desktop\\Evecon\\!Evecon\\dev" or\
        curPath == "C:\\Users\\Mini-Pc Nutzer.000\\Desktop\\Evecon\\!Evecon\\Evecon" or \
        curPath == "C:\\Users\\Mini-Pc Nutzer.000\\Desktop\\Evecon":  # variant two: predefined Windows dir
    environmentPath = "C:\\Users\\Mini-Pc Nutzer.000\\Desktop\\Evecon"
elif curPathSplit[-2] == "!Evecon":
    if curPathSplit[-1] == "Evecon" or curPathSplit[-1] == "!Console" or curPathSplit[-1] == "Exe" or curPathSplit[-1] == "dev":  # remove dev will be deprecated
        environmentPath = ""
        for x in range(len(curPathSplit)-2):
            environmentPath += curPathSplit[x] + path_seg
        environmentPath = environmentPath.rstrip(path_seg)
else:
    print("No Environmentpath found")
    environmentPath = curPath

del curPath, curPathSplit

# noinspection PyUnboundLocalVariable
environmentPath = environmentPath.rstrip(path_seg) + path_seg


def reset_paths():
    """
    resets the path variables
    use it if the environmentPath has been updated
    """
    global infoPath, versionFile, changelogFile, notiePath, outputPath, tmpPath, usedPortsFile, dataPath, nheeDir, foxiDir, configPath, splWeapFile, MusicFile, deacSSFile, logFile, backupPath, backupMusicFile, icoPath, radioIcoFile

    infoPath = environmentPath + "data" + path_seg + "Info" + path_seg
    versionFile = infoPath + "version"
    changelogFile = infoPath + "Changelog.txt"

    notiePath = environmentPath + "data" + path_seg + "Noties"
    outputPath = environmentPath + "data" + path_seg + "Output"

    tmpPath = environmentPath + "data" + path_seg + "tmp" + path_seg

    usedPortsFile = tmpPath + "usedPorts.txt"

    dataPath = environmentPath + "data" + path_seg + "Data" + path_seg

    nheeDir = dataPath + "nhee" + path_seg
    foxiDir = dataPath + "foxi" + path_seg

    configPath = environmentPath + "data" + path_seg + "Config" + path_seg

    splWeapFile = configPath + "splWeap.json"
    MusicFile = configPath + "Music.json"
    deacSSFile = configPath + "deacSS"

    logFile = environmentPath + "data" + path_seg + "log.txt"

    backupPath = environmentPath + "data" + path_seg + "Backup" +path_seg

    backupMusicFile = backupPath + "Music.json"

    icoPath = environmentPath + "data" + path_seg + "Ico" + path_seg
    radioIcoFile = icoPath + "Radio.ico"



def refreshVersion():
    global file_versions, file_version
    if not os.path.exists(versionFile):
        file_versions = [0, "0.0.0.0", "0.0.0.0"]
        file_version = "0.0.0.0"
        return
    with open(versionFile, "r") as file_version_raw:
        file_versions = file_version_raw.readlines()

    file_versions.append(code_version)

    file_version = file_versions[1]


validEnv = False


def testEnv():
    global validEnv

    if os.path.exists(usedPortsFile) and os.path.exists(backupMusicFile):
        validEnv = True
        return True
    else:
        validEnv = False

        print("Invalid environment: " + environmentPath)
        if not suppressErrors:
            input("You can go on (Enter), but something could happen")
        return False


# noinspection PyGlobalUndefined
def readConfig():
    global browser, musicrandom, enable_FoxNhe, thisIP, cores, foxORnhe, firefox_path, vivaldi_path
    config = configparser.ConfigParser()
    config.read(configPath + "config.ini")

    try:
        enable_FoxNhe_tmp = config["FoxNhe"]["enable_FoxNhe"]
        if enable_FoxNhe_tmp == "True":
            enable_FoxNhe = True
        elif enable_FoxNhe_tmp == "False":
            enable_FoxNhe = False
        foxORnhe = config["FoxNhe"]["foxORnhe"]

        musicrandom_tmp = config["Music"]["random"]
        if musicrandom_tmp == "True":
            musicrandom = True
        elif musicrandom_tmp == "False":
            musicrandom = False

        thisIP = config["PC"]["thisIP"]

        cores = int(config["PC"]["cores"])

        browser = config["Notepad"]["browser"]
        firefox_path = config["Browser"]["firefox_path"]
        vivaldi_path = config["Browser"]["vivaldi_path"]
    except KeyError:
        if suppressErrors:
            print("Config Error: something crashed!")
        else:
            input("Config Error: something crashed!\nYou can go on (Enter), but something could happen")


if os.path.exists("!Console.py") and os.path.exists("EveconLib"):
    myType = "python_file"
elif os.path.exists("!Console.exe") and not os.path.exists("!Console.exe.manifest"):
    myType = "standalone_exe"
elif os.path.exists("!Console.exe") and os.path.exists("!Console.exe.manifest"):
    myType = "lib_exe"


Computername = socket.gethostname()

if Computername == "Computer-Testet":
    computer = "MiniPC"
    HomePC = True

elif Computername == "Bigger-PC":
    computer = "BigPC"
    HomePC = True

elif Computername == "Test":
    computer = "AldiPC"

elif Computername == "TosLaptop":
    computer = "TosLaptop"

else:
    computer = None

reset_paths()


refreshVersion()

# test environment
testEnv()

# read config

readConfig()

