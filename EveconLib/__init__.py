import os
import sys
# set dir
orgDir = os.getcwd()

if sys.platform == "win32":
    path_seg = "\\"

    # adding here (for pyglet) the ffmpeg libs
    if os.path.exists(r"C:\Dev\ffmpeg\bin"):
        os.environ["PATH"] += r";C:\Dev\ffmpeg\bin"
else:
    path_seg = "/"

# loading config, this will check the environment
import EveconLib.Config

import EveconLib.EveconExceptions

import EveconLib.Tools
import EveconLib.Games
import EveconLib.Networking
import EveconLib.Programs

# starting startup functions

EveconLib.Programs.Startup.Startup.startup()
