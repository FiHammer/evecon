from pyglet.media import Player as PygletPlayer, load as load_media
import threading
import time


class Player:
    def __init__(self):
        self.player = PygletPlayer()
        self.thread = PlayerThread(self.player)

    def getVolume(self) -> float:
        return self.player.volume

    def setVolume(self, volume: float):
        self.player.volume = volume

    volume = property(getVolume, setVolume)

    def getTime(self):
        return self.player.time

    time = property(getTime)

    def play(self):
        if self.thread.started:  # unpause
            self.thread.pause = False
        else:
            self.thread.start()  # start playing

    def pause(self):
        self.thread.pause = True

    def delete(self):
        self.thread.pause = False
        self.thread.playing = False
        self.thread.join(5)  # max five seconds
        self.player.delete()

    def queue(self, source):
        try:
            self.player.queue(source)
        except Exception:  # no new item
            print("Rip 1")

    def next_source(self):
        try:
            self.player.next_source()
        except AssertionError:  # no new item
            print("Rip 2")


class PlayerThread(threading.Thread):
    def __init__(self, player: PygletPlayer):
        super().__init__()
        self.player = player
        self.playing = False
        self.pause = False
        self.started = False

    def run(self):
        self.started = True
        self.playing = True
        # self.player.play()

        while self.playing:
            try:
                self.player.play()
                # self.player._set_playing(True)  # or sth else
                if self.pause:
                    self.player.pause()
                    while self.pause:
                        time.sleep(0.2)
                    self.player.play()
            except Exception:
                print("RIP RIP")
        self.pause = False


