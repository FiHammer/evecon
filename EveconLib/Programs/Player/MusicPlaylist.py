"""
three layer playlist
"""
from typing import List, Any, Callable
import EveconLib
import random

try:
    MusicFile = EveconLib.Programs.Player.MusicFileLoaderModule.MusicFile
    MusicKey = EveconLib.Programs.Player.MusicFileLoaderModule.MusicKey
except AttributeError:
    MusicFile = Any
    MusicKey = Any


class Playlist:
    def __init__(self, onPlFile0Change: Callable = None):
        self.originalList: List[MusicFile] = []  # mainList with all musicFiles

        self.playlist: List[MusicFile] = []  # mainList for the musicplayer
        self.editList = EditableList(self, my_type="edit")    # mainList for editing
        self.showList = EditableList(self, my_type="show")    # mainList only for searching and Co.

        self.playlistLast: List[MusicFile] = []  # simple last playlist list

        self.onPlFileZeroChangeFunc = onPlFile0Change  # this method will be called if the first file of the playlist will be changed!

        # settings
        self.activeLayer = "play"

    def getPlaylist(self) -> List[MusicFile]:
        return self.playlist

    def __getitem__(self, i: int):
        return self.getCurList()[i]

    def index(self, file: MusicFile) -> int:
        return self.getCurList().index(file)

    def onPlFileZeroChange(self):
        if not self.onPlFileZeroChangeFunc:
            return
        try:
            self.onPlFileZeroChangeFunc(True)  # next function from mp
        except TypeError:
            self.onPlFileZeroChangeFunc()

    def applyToPlaylist(self, overwrite_file_zero=True):
        self.playlistLast = self.playlist.copy()
        if self.playlist[0] == self.editList[0]:
            for file in self.editList.mainList:
                file.active = True
            self.playlist = self.editList.mainList
            return   # first file did not change

        if not overwrite_file_zero and self.playlist:  # empty pl would be bad
            pl_file_zero = self.playlist[0]
            pl = self.editList.mainList
            if pl_file_zero in pl:
                pl.remove(pl_file_zero)

            for file in pl:
                file.active = True

            self.playlist: List[MusicFile] = [pl_file_zero] + pl
        else:
            for file in self.editList.mainList:
                file.active = True
            self.playlist = self.editList.mainList
            self.onPlFileZeroChange()

    def applyListToPlaylist(self, list_: List[MusicFile], overwrite_file_zero=True):
        self.playlistLast = self.playlist.copy()
        if self.playlist[0] == list_[0]:
            for file in list_:
                file.active = True
            self.playlist = list_.copy()
            return   # first file did not change

        if not overwrite_file_zero and self.playlist:  # empty pl would be bad
            pl_file_zero = self.playlist[0]
            pl = list_.copy()
            if pl_file_zero in pl:
                pl.remove(pl_file_zero)

            for file in pl:
                file.active = True

            self.playlist: List[MusicFile] = [pl_file_zero] + pl
        else:
            for file in list_:
                file.active = True
            self.playlist = list_
            self.onPlFileZeroChange()

    def switchToPlaylist(self):
        self.activeLayer = "play"

    def switchToEdit(self, remake=True):
        if remake:
            self.editList.makePlaylist()  # normal list
        self.activeLayer = "edit"

    def switchToShow(self):
        self.showList.makeOriginal()  # normal list
        self.activeLayer = "show"

    def append(self, file: MusicFile, refresh=True):
        if file in self.originalList:
            return
        self.originalList.append(file)
        if file not in self.playlist:
            if isinstance(file, EveconLib.Programs.Player.MusicFileLoaderModule.MusicFile):
                file.active = True
            else:  # some debug for other objects
                self.playlist.append(file)

        if refresh:
            self.refresh()

    def pl_removeFile(self, file: MusicFile):
        """
        removes a file from the playlist

        please do not call this method, it will only be called BY THE MusicFile
        """
        self.playlistLast = self.playlist.copy()
        if file not in self.playlist:
            return
        self.playlist.remove(file)

    def pl_addFile(self, file: MusicFile):
        """
        adds a file to the playlist

        please do not call this method, it will only be called BY THE MusicFile
        """
        self.playlistLast = self.playlist.copy()
        if file in self.playlist:
            return
        self.playlist.append(file)

    def pl_makeList(self):
        for file in self.originalList:
            file.active = False
        for file in self.originalList:
            file.active = True

        self.playlistLast = self.playlist.copy()

    def pl_nextTrack(self):
        self.playlist.append(self.playlist[0])
        del self.playlist[0]

    # special methods

    def getCurList(self):
        if self.activeLayer == "edit":
            return self.editList
        elif self.activeLayer == "show":
            return self.showList
        elif self.activeLayer == "play":
            return self.playlist

    def refresh(self):
        cur_list: EditableList = self.getCurList()
        if not isinstance(cur_list, EditableList):
            return
        cur_list.refresh()

    def reset(self, refresh=True):
        cur_list: EditableList = self.getCurList()
        if isinstance(cur_list, EditableList):
            return
        cur_list.resetFilers(refresh=False)
        cur_list.resetSort()

        if refresh:
            cur_list.refresh()

    def pl_shuffle(self, overwriteFileZero=False, overwrite_previous=True, reset_sort=True):
        """
        shuffles the editList and applies it INSTANTLY to the playlist
        """
        self.editList.makePlaylist()  # normal list
        self.editList.shuffle(overwriteFileZero=overwriteFileZero, overwrite_previous=overwrite_previous, reset_sort=reset_sort)
        self.applyToPlaylist(overwrite_file_zero=overwriteFileZero)

    def pl_redoList(self):
        """
        overwrite the mainlist to the previous mainlist
        """
        self.applyListToPlaylist(self.playlistLast, overwrite_file_zero=True)

    def pl_moveToZero(self, file: MusicFile):
        """
        moves a file to the front/ first position
        :param file: the music file
        """
        if file == self.playlist[0]:
            return

        self.editList.makePlaylist()  # normal list
        self.editList.moveFileToPos(file, 0)
        self.applyToPlaylist(overwrite_file_zero=True)


class EditableList:
    """
    with better life features such as
        filter and search, previous backup, random
    """
    validTypes = ["date", "name", "musickey", "none", "original", "playlist", "antitle", "aninterpreter", "anmusictype", "anname"]

    def getFullList(self) -> List[MusicFile]:
        return self.superContainer.originalList

    originalList = property(getFullList)

    def getPlayList(self) -> List[MusicFile]:
        return self.superContainer.playlist

    playlist = property(getPlayList)

    def getBeginList(self) -> List[MusicFile]:
        if self.myType == "show":
            return self.originalList
        elif self.myType == "edit":
            return self.playlist
        else:
            return self.originalList

    beginList = property(getBeginList)

    def __getitem__(self, i: int) -> MusicFile:
        return self.mainList[i]

    def index(self, file: MusicFile) -> int:
        return self.mainList.index(file)

    def insert(self, index: int, file: MusicFile):
        self.mainList.insert(index, file)

    def __len__(self):
        return self.mainList.__len__()

    def __init__(self, playlist: Playlist, start_list: List[MusicFile] = None, my_type=""):
        if start_list is None:
            start_list = []
        self.mainList = start_list
        self.lastList = start_list

        # 'show', 'edit'; showlist prefers the original list and editlist the playlist
        self.myType = my_type

        self.superContainer = playlist

        self.activeFilers = []
        self.sortBy = "none"
        self.searchList = {}

        self._hiddenFiles: List[MusicFile] = []  # contains hidden files, one type of filtering

    def append(self, file: MusicFile):
        self._writeToLastList()
        self.mainList.append(file)

    def remove(self, file: MusicFile):
        self._writeToLastList()
        self.mainList.remove(file)

    def refresh(self):
        """
        re-sorts the mainlist
        """
        self._writeToLastList()
        self.sort(overwrite_previous=False, nofilters=True)
        self.applyFilters(overwrite_previous=False)

    def resetSort(self):
        self.sortBy = "none"

    def reset(self, refresh=True):
        self.resetFilers(refresh=False)
        self.resetSort()

        if refresh:
            self.refresh()

    def resetFilers(self, refresh=True):
        self.activeFilers.clear()
        self.searchList.clear()
        if refresh:
            self.applyFilters()

    def deactivateSearch(self, refresh=True):
        self.searchList.clear()
        if "search" in self.validTypes:
            self.validTypes.remove("search")

        if refresh:
            self.applyFilters()

    def makeOriginal(self):
        self.resetFilers(refresh=False)
        self.mainList = self.originalList.copy()

    def makePlaylist(self):
        self.resetFilers(refresh=False)
        self.mainList = self.playlist.copy()

    def _writeToLastList(self):
        self.lastList = self.mainList.copy()

    def redoList(self):
        """
        overwrite the mainlist to the previous mainlist
        """
        self.mainList = self.lastList.copy()
        self._writeToLastList()

    def applyFilters(self, overwrite_previous=True, use_begin_list=True):
        """
        manual apply the filters

        filters:
         search
         hide (ignore mainList)
         playlist

        this will not sort the mainList, it will only add or delete musicfiles!

        :param overwrite_previous: -
        :param use_begin_list: important this MUST be active if searching with a smaller str than previous
        """

        if not self.activeFilers:  # no filters active
            return
        if overwrite_previous:
            self._writeToLastList()

        if use_begin_list:
            self.mainList = self.beginList.copy()  # begin with the original mainList

        if "hide" in self.activeFilers:  # use the hide mainList
            for file in self._hiddenFiles:
                if file in self.mainList:
                    self.mainList.remove(file)

        if "playlist" in self.activeFilers:  # use the playlist mainList (invert)
            for file in self.mainList:
                if file not in self.playlist:
                    self.mainList.remove(file)

        if "search" in self.activeFilers and self.searchList:  # search activated
            for search_type in self.searchList:
                for file in EveconLib.Tools.SearchBetter(self.searchList[search_type], self.mainList,
                                                         return_object=True, only_once=True, lower=True,
                                                         invert_output=True, use_fnmatch=True):  # use every time fnmat?
                    self.mainList.remove(file)

    def sort(self, new_type="", overwrite_previous=True, nofilters=False):
        """
        sorts the mainList

        types:
            date?
            name
            musickey
            none = ignore
            random
            original = copy of original mainList
            playlist

            anstuff:
                antitle
                aninterpreter
                anmusictype
                anname

        type in self.sortBy

        nofilter: do not automatically apply the filters for the mode "original"
        """
        if overwrite_previous:
            self._writeToLastList()

        if new_type:
            if new_type in self.validTypes:  # not valid
                self.sortBy = new_type
            else:
                self.sortBy = "none"

        sort_list = []
        nosort_list = []  # will be sorted and then appended at the end
        if self.sortBy == "none":
            return
        elif self.sortBy == "playlist":  # use unsorted mainList
            new_list = []
            for file in self.playlist:
                if file in self.mainList:
                    new_list.append(file)

            self.mainList = new_list
            if not nofilters:
                self.applyFilters(False)
        elif self.sortBy == "original":  # use unsorted mainList
            new_list = []
            for file in self.originalList:
                if file in self.mainList:
                    new_list.append(file)

            self.mainList = new_list
            if not nofilters:
                self.applyFilters(False)
        elif self.sortBy == "date":
            for file in self.mainList:
                s = EveconLib.Tools.StrPlusData(file.time)
                s.additionalData = file
                sort_list.append(s)
        elif self.sortBy == "name":
            for file in self.mainList:
                s = EveconLib.Tools.StrPlusData(file.name)
                s.additionalData = file
                sort_list.append(s)
        elif self.sortBy == "musickey":
            for file in self.mainList:
                s = EveconLib.Tools.StrPlusData(file.parentKey)
                s.additionalData = file
                sort_list.append(s)

        # andata stuff

        elif self.sortBy == "antitle":
            for file in self.mainList:
                if file.anData["valid"] and file.anData.get("title"):
                    s = EveconLib.Tools.StrPlusData(file.anData["title"])
                    s.additionalData = file
                    sort_list.append(s)
                else:
                    s = EveconLib.Tools.StrPlusData(file.name)
                    s.additionalData = file
                    nosort_list.append(s)

        elif self.sortBy == "aninterpreter":
            for file in self.mainList:
                if file.anData["valid"] and file.anData.get("interpreter"):
                    s = EveconLib.Tools.StrPlusData(file.anData["interpreter"])
                    s.additionalData = file
                    sort_list.append(s)
                else:
                    s = EveconLib.Tools.StrPlusData(file.name)
                    s.additionalData = file
                    nosort_list.append(s)

        elif self.sortBy == "anname":
            for file in self.mainList:
                if file.anData["valid"] and file.anData.get("animeName"):
                    s = EveconLib.Tools.StrPlusData(file.anData["animeName"])
                    s.additionalData = file
                    sort_list.append(s)
                else:
                    s = EveconLib.Tools.StrPlusData(file.name)
                    s.additionalData = file
                    nosort_list.append(s)

        elif self.sortBy == "anmusictype":
            for file in self.mainList:
                if file.anData["valid"] and file.anData.get("musictype"):
                    s = EveconLib.Tools.StrPlusData(file.anData["musictype"])
                    s.additionalData = file
                    sort_list.append(s)
                else:
                    s = EveconLib.Tools.StrPlusData(file.name)
                    s.additionalData = file
                    nosort_list.append(s)
        else:
            return
        nosort_list.sort()
        sort_list.sort()
        new_list = []
        for file in sort_list:
            new_list.append(file.additionalData)
        for file in nosort_list:
            new_list.append(file.additionalData)
        self.mainList = new_list

    def search(self, key, search_type="name", clear_previous=False, refresh=True, searchInner=True):
        """
        searches in the main mainList, search will be refreshed on every self.refresh

        :param key: search key
        :param search_type: type of the search, where to search; possibilities in self.validTypes
        :param clear_previous: clear all searches, including other types
        :param refresh: refresh the mainList
        :param searchInner: use a * on the end and at the begin
        """
        if search_type not in self.validTypes or search_type == "none":
            return  # not valid search_type
        if clear_previous:
            self.deactivateSearch(False)
        if "search" not in self.activeFilers:
            self.activeFilers.append("search")

        if searchInner:
            key = "*" + key + "*"

        self.searchList[search_type] = key

        if refresh:
            self.applyFilters()

    def hideFile(self, file: MusicFile, refresh=True, activate_hide=True):
        if file in self._hiddenFiles and file not in self.mainList:  # file should be in mainlist and not be hidden
            return

        if "hide" not in self.activeFilers and activate_hide:
            self.activeFilers.append("hide")

        self._hiddenFiles.append(file)

        if refresh:
            self.applyFilters()

    def showFile(self, file: MusicFile, refresh=True, activate_hide=True):
        if file not in self._hiddenFiles and file in self.mainList:  # file should not be in mainlist and be hidden
            return

        if "hide" not in self.activeFilers and activate_hide:
            self.activeFilers.append("hide")

        self._hiddenFiles.remove(file)

        if refresh:
            self.applyFilters()

    def hideKey(self, key: MusicKey, refresh=True):
        for file in key.getExtChildrenFiles():
            self.hideFile(file, refresh=False)

        if refresh:
            self.applyFilters()

    def showKey(self, key: MusicKey, refresh=True):
        for file in key.getExtChildrenFiles():
            self.showFile(file, refresh=False)

        if refresh:
            self.applyFilters()

    def swapFiles(self, file1: MusicFile, file2: MusicFile):
        self._writeToLastList()
        index1 = self.mainList.index(file1)
        index2 = self.mainList.index(file2)
        self.moveFileToPos(file1, index2)
        self.moveFileToPos(file2, index1)

    def moveFileToPos(self, file: MusicFile, new_pos: int, constant_pos=False):
        """

        :param file: musicfile
        :param new_pos: new position
        :param constant_pos: should be the pos constant after deleting the file? =>
                             move file to the end of the file -1 position = False or -0 position = True
                             [1,2,3,4,5,6] move '2' -> pos: 3 ('4'):
                                constant_pow = False => [1,3,4,2,5,6]
                                             = True  => [1,3,4,5,2,6]
        """
        if self.mainList.index(file) == new_pos:
            return
        if self.mainList.index(file) < new_pos and not constant_pos:
            ...  # new_pos -= 1  # maybe it is not needed

        if file in self.mainList:
            self.mainList.remove(file)
        self.addFileToPos(file, new_pos)

    def moveFileToEnd(self, file: MusicFile):
        self.moveFileToPos(file, len(self.mainList)-1)

    def addFileToPos(self, file: MusicFile, new_pos: int):
        if file in self.mainList:  # auto remove
            self.mainList.remove(file)

        self.mainList.insert(new_pos, file)

    def shuffle(self, overwriteFileZero=True, overwrite_previous=True, reset_sort=True):
        if reset_sort:
            self.resetSort()  # so this will not be overwritten in the next step
        if overwrite_previous:
            self._writeToLastList()
        if overwriteFileZero:
            random.shuffle(self.mainList)
        else:
            ml = self.mainList.copy()
            ml_file_zero = self.mainList[0]
            del ml[0]

            random.shuffle(ml)
            self.mainList: List[MusicFile] = [ml_file_zero] + ml

