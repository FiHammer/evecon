import threading
import pyglet
import sys
import time
import subprocess
import socket
from typing import Any, List

import EveconLib

try:
    MusicFile = EveconLib.Programs.Player.MusicFileLoaderModule.MusicFile
    # MusicFilelist = EveconLib.Programs.Player.MusicPlaylist.MusicFileList
except AttributeError:
    MusicFile = Any
    Player = Any
# print(pyglet.version)
if "1.4" <= pyglet.version < "1.5":
    NEW_PLAYER = True
else:
    NEW_PLAYER = False


# not noinspection PyTypeChecker
class MusicPlayer(threading.Thread):
    def __init__(self, systray=True, randomize=True, expandRange=2, stop_del=False, scanner_active=True, balloonTip=True,
                 killMeAfterEnd=True, remote=True, remotePort=4554, selfprint=False, specialFilePath=None, neverPrint=False,
                 autoPlayVideo=True, simple_input=False, auto_print=True):
        super().__init__()

        self.debug = False

        self.find_music_out = {}

        self.systray = None
        self.systrayon = systray
        self.balloonTip = balloonTip
        self.killMeAfterEnd = killMeAfterEnd
        self.remote = remote
        self.remotePort = remotePort
        self.selfprint = selfprint
        self.neverPrint = neverPrint

        self.remoteAddress = ""
        self.remoteAction = ""
        if self.remote:
            self.server = EveconLib.Networking.Server(port=self.remotePort, ip=EveconLib.Config.thisIP, stdReact=self.react_remote, printLog=False)
        else:
            self.server = None

        self.volume = EveconLib.Tools.Windows.Volume.getVolume()
        self.volumep = 0.5

        # stop the musicplayer while hovering over file 1 and pressing 'del'-button
        self.stop_del = stop_del
        self.randomizer = randomize
        self.scanner_active = scanner_active
        self.remoteActivated = remote  # WIP
        EveconLib.Config.musicrun = True

        self.starttime = 0
        self.hardworktime = 0
        self.musicrun = False

        self.mainList = EveconLib.Programs.Player.Playlist(onPlFile0Change=self.next)
        # self.superContainer = []
        # self.last_playlist = []

        # self.search_list = []

        self.running = False
        self.playing = False
        self.exitn = False
        self.allowPrint = False
        self.autorefresh = auto_print  # Todo add own autoprinter

        if NEW_PLAYER:
            self.player = EveconLib.Programs.Player.Player()
        else:
            import pyglet
            import pyglet.media
            self.player = pyglet.media.Player()  # TODO change
        self.timer = EveconLib.Tools.Timer()
        self.scanner = EveconLib.Programs.Scanner(self.react, simple_input=simple_input)
        self.spl = EveconLib.Programs.SplWeapRand()
        self.rhy = EveconLib.Games.Rhythm.Game()

        self.skip_del = False
        self.paused = False
        self.pause_type = ""
        self.muted = False
        self.mute_vol = 1
        self.con_main = "pl"
        self.con_main_last = None
        self.con_cont = "set"  # edit means edit AND show list
        self.change = ""

        self.last_print = 0
        self.last_print_auto = 0

        self.cur_Input = ""
        self.cur_Pos = 0
        self.expandRange = expandRange

        self.editing = False  # this is edit AND show list
        self.cur_Search = ""

        self.arrowSetting = [(3, 15, 3), (16, -1, 6)]  # (minPress, maxPress (-1 = infinite), Plus)

        self.notifications = []

        self.tmp_pl_input_1 = []
        self.tmp_pl_input_2 = []
        self.tmp_pl_output_1 = []
        self.tmp_pl_output_2 = []

        self.musicKeysLeft = []

        self.lastPresses = [0, time.time(), "None"]

        self.mfl = EveconLib.Programs.Player.MusicFileLoader(self.notify, self.neverPrint,
                                                             specialFilePath=specialFilePath,
                                                             musicPlayerPlaylist=self.mainList)

        self.videoPlayerClient = None  # client for videoPlayer
        self.autoPlayVideo = autoPlayVideo
        self.videoPlayerIsPlaying = False
        self.videoPlayerProcess = None
        self.waitForVPstart = False

        self.sub = None
        self.breakThisPlay = False  # this is a debug var to stop the main thread from waiting, normaly done with playing

    # current list
    def idToF(self, i: int) -> MusicFile:
        return self.mainList[i]

    def fToId(self, file: MusicFile) -> int:
        return self.mainList.index(file)

    # only playlist
    def idToPLF(self, i: int) -> MusicFile:
        return self.mainList.playlist[i]

    def fToPLId(self, file: MusicFile) -> int:
        return self.mainList.playlist.index(file)

    def getPl(self) -> List[MusicFile]:
        return self.mainList.getPlaylist()

    def getCur(self) -> MusicFile:
        if len(self.playlist) == 0:
            return
            # self.stop()
        return self.playlist[0]

    playlist = property(getPl)

    def addMusic(self, key, cusPath=False, printStaMSG=True, printEndMSG=True,
                 make_notification=False, load_music=True, threadLoad=True):  # key (AN, LIS)
        """
        :param key: the key of the id (normal id, mpl id)
        :param cusPath: defines the path for a custom path (ignores the key)
        :param printStaMSG: clears the screen and prints the start msg
        :param printEndMSG: prints the finished msg
        :param make_notification: make a notification after finishing
        :param load_music: load music (currently for pyglet)
        :param threadLoad: load the music in multiple threads
        :return: success
        """
        return self.mfl.addMusic(key=key, cusPath=cusPath, printStaMSG=printStaMSG,
                                 printEndMSG=printEndMSG, makeNoti=make_notification,
                                 loadForPyglet=load_music, threadLoad=threadLoad)

    def read_musiclist(self):
        self.mfl.refreshMusicList()

    def resetInterface(self):
        self.con_main = "pl"
        self.con_cont = "set"
        self.cur_Input = ""
        self.editing = False
        self.cur_Pos = 0
        self.change = ""
        # self.notifications = []  # really?

    def reloadMusic(self, track_num: int):
        self.mfl.reloadFile(track_num)

    def make_playlist(self):
        # self.last_playlist = self.superContainer.copy()

        for file in self.mfl.files_allFiles:
            self.mainList.append(file)

        self.mainList.pl_makeList()

        if self.randomizer:
            self.mainList.pl_shuffle(overwriteFileZero=True, overwrite_previous=True)  # overwrite previous needed?
            self.mainList.playlistLast = self.mainList.playlist.copy()  # make the non sorted list non existing
        self.resetInterface()

    def restoreLastList(self):
        lis = self.mainList.getCurList()
        if not isinstance(lis, EveconLib.Programs.Player.MusicPlaylist.EditableList):
            return self.mainList.pl_redoList()
        lis.redoList()

    def refreshTitle(self):
        if self.getCur().anData["valid"]:
            EveconLib.Tools.title("OLD", self.getCur().anData["title"], "Now Playing")
        else:
            EveconLib.Tools.title("OLD", self.getCur().name, "Now Playing")

    def refresh(self, title=False, print_me=True):
        if title:
            self.refreshTitle()
        if print_me:
            self.printit()

    def showBalloonTip(self):
        if self.getCur().anData["valid"]:
            name = self.getCur().anData["title"]
        else:
            name = self.getCur().name
        EveconLib.Tools.Windows.BalloonTip("Evecon: MusicPlayer", "Now playing: " + name)

    # Options

    def play(self, fromServer=False):
        if self.videoPlayerIsPlaying and not fromServer:  # server react
            self.videoPlayerClient.send("play")

        self.paused = False
        self.player.play()
        self.hardworktime = time.time() + 0.2

    def pause(self, fromServer=False):
        if self.videoPlayerIsPlaying and not fromServer:  # server react
            self.videoPlayerClient.send("pause")

        self.paused = True
        self.player.pause()
        self.hardworktime = time.time() + 0.2

    def switch(self):
        if self.paused:
            self.play()
        else:
            self.pause()

    def switchmute(self):
        if self.muted:
            self.unmute()
        else:
            self.mute()

    def mute(self, fromServer=False):
        if self.videoPlayerIsPlaying and not fromServer:  # server react
            self.videoPlayerClient.send("mute")
        self.muted = True
        self.mute_vol = self.volumep
        self.volp(0)

    def unmute(self, fromServer=False):
        if self.videoPlayerIsPlaying and not fromServer:  # server react
            self.videoPlayerClient.send("unmute")
        self.muted = False
        self.volp(self.mute_vol)

    def stop(self):
        # noinspection PyGlobalUndefined
        EveconLib.Config.musicrun = False

        if self.videoPlayerIsPlaying:
            self.stopVideoPlayer()

        self.musicrun = False
        self.playing = False
        self.paused = False
        self.running = False
        self.scanner.running = False
        self.player.delete()

        if self.remote:
            self.server.exit()
        if self.systrayon and self.killMeAfterEnd:
            time.sleep(1)
            EveconLib.Tools.killme()

    def __del__(self):
        if self.videoPlayerIsPlaying:
            self.videoPlayerProcess.kill()

    def next(self, skipthis=False, fromServer=False, forceKillVideo=False):
        if self.waitForVPstart and forceKillVideo:
            self.stopVideoPlayer(from_server=True, force=True)  # do as a server to terminate the vp completly
        elif self.waitForVPstart:
            return
        elif self.videoPlayerIsPlaying:  # server react
            self.stopVideoPlayer(fromServer)

        if skipthis:
            self.skip_del = True
        self.playing = False
        if self.paused:
            self.paused = False
            # self.player.play()  # TODO HERE COULD BE A MISTAKE (COMMENTED TO TRY IF THIS CAUSES ERRORS)
        self.hardworktime = time.time() + 0.2

        if self.waitForVPstart and forceKillVideo:  # doing things, normally the main thread would do
            self.breakThisPlay = True

    def DelByKey(self, key):
        """
        this will delete musicfiles from the superContainer

        :param key: the key from the musicFileEditor
        :return:
        """

        key_obj = self.mfl.getK(key)
        if not key_obj:
            return  # not found

        if not key_obj.active:
            return  # already deleted
        next_file = False
        if self.getCur() in key_obj.getExtChildrenFiles():
            next_file = True

        key_obj.active = False

        if next_file:
            self.next(True)

    def vol(self, vol):
        self.volume = vol
        EveconLib.Tools.Windows.Volume.change(vol)

    def volp(self, vol, fromServer=False):
        if self.videoPlayerIsPlaying and not fromServer:  # server react
            self.videoPlayerClient.send("vol_"+str(vol))

        self.volumep = vol
        self.player.volume = self.volumep

    def run(self):
        if not self.mfl.files_allFiles:
            return  # empty list => first add something
        if self.systrayon and sys.platform == "win32":
            def quitFunc(y):
                if y:
                    pass
                self.stop()

            def unp_p(y):
                if y:
                    pass
                self.switch()

            def nextm(y):
                if y:
                    pass
                self.next()

            def delm(y):
                if y:
                    pass
                self.idToPLF(0).active = False
                self.next(True)
                # self.playing = False
                # if self.paused:
                #     self.play()

            def reroll(y):
                if y:
                    pass
                self.mainList.pl_shuffle()

            def vol01(y):
                if y:
                    pass
                self.volp(0.1)

            def vol025(y):
                if y:
                    pass
                self.volp(0.25)

            def vol05(y):
                if y:
                    pass
                self.volp(0.5)

            def vol1(y):
                if y:
                    pass
                self.volp(1)

            sub_menu1 = {"0.1": vol01, "0.25": vol025, "0.5": vol05, "1": vol1}

            self.systray = EveconLib.Tools.Windows.SysTray(EveconLib.Config.radioIcoFile, "Evecon: MusicPlayer",
                                                           {"Pause/Unpause": unp_p, "Next": nextm,
                                                            "Del": delm, "Reroll": reroll},
                                                           sub_menu1=sub_menu1, sub_menu_name1="Volume",
                                                           quitFunc=quitFunc)
            self.systray.start()

        self.make_playlist()

        if self.playlist:  # activation condition
            self.musicrun = True
        else:
            print(self.mainList)

        if self.remote:
            self.server.start()

        self.starttime = time.time()
        self.hardworktime = time.time()
        if self.scanner_active:
            self.scanner.start()

        self.player.volume = self.volumep

        while self.musicrun:
            if not self.getCur().pygletData:
                self.getCur().loadForPyglet(threadLoad=False)
            try:
                if self.getCur().pygletData._is_queued:
                    self.getCur().loadForPyglet()
            except AttributeError:
                pass

            if self.balloonTip:
                self.showBalloonTip()

            if self.getCur().subAvail:
                self.sub = EveconLib.Programs.Player.SubTitleParser(self.getCur().subFile)
            else:
                self.sub = None

            # VIDEO PLAYER TEST
            if self.getCur().type == "video" and self.autoPlayVideo:
                self.callVideoPlayer()  # ATTENTION: THE VIDEO IS NOW PLAYING SO NORMAL PLAYER IS PAUSED

                while self.waitForVPstart:  # wait loop for main player thread
                    time.sleep(0.25)

            else:
                self.player.queue(self.getCur().pygletData)

                if not self.paused:
                    self.player.play()

            self.timer.start()  # music timer

            self.running = True
            self.playing = True

            self.allowPrint = True
            self.refreshTitle()

            self.printit()
            self.last_print_auto = time.time()
            while self.playing:
                if self.breakThisPlay:
                    self.breakThisPlay = False
                    break

                time.sleep(0.15)
                for x in range(5):
                    if self.videoPlayerIsPlaying:
                        if round(self.getCur().pygletData.duration) <= round(self.timer.getTime()) - 3:  # three seconds wait time for async
                            self.playing = False
                        time.sleep(0.1)  # easy continue

                    elif self.player.time == 0:
                        self.playing = False
                    elif round(self.getCur().pygletData.duration) <= round(self.timer.getTime()):
                        self.playing = False
                    time.sleep(0.2)

                    self.refresh(title=False, print_me=self.selfprint)

                while self.paused:
                    self.timer.pause()

                    while self.paused:
                        time.sleep(0.25)

                    self.timer.unpause()
                    self.refresh(title=True, print_me=self.selfprint)

            if self.videoPlayerIsPlaying:
                self.stopVideoPlayer()
            else:
                self.player.next_source()

            self.timer.reset()

            if self.skip_del:
                self.skip_del = False
            else:
                self.mainList.pl_nextTrack()

            self.running = False

            if self.exitn:
                self.stop()

    def notify(self, msg, title=None, screentime=5.0, maxTime=None):
        found = False
        if title:
            for noti in self.notifications:
                if noti["title"] == title:
                    found = True
                    noti["msgs"].append({
                        "msg": msg,
                        "screentime": screentime,
                        "starttime": time.time()
                    })
                    break
        if not found:
            self.notifications.append({
                "msgs": [{
                    "msg": msg,
                    "screentime": screentime,
                    "starttime": time.time()
                }],
                "maxTime": maxTime,
                "maxTimestart": time.time(),
                "title": title
            })
        if self.autorefresh and self.running:
            self.printit()

    def return_head_info(self):
        # Info-Container

        output_list = []

        if not self.remoteAddress:
            output_list.append("Musicplayer: \n")
        else:
            if self.remoteAddress == "192.168.2.103":
                con = "FP2"
            else:
                con = self.remoteAddress
            output_list.append("Musicplayer: (%s connected)\n" % con)

        playword = "Playing"

        if self.getCur().type == "video":
            playword = "Playing video"

        if self.getCur().anData["valid"]:
            output_list.append(
                playword + ": \n%s \nFrom %s" % (self.getCur().anData["title"], self.getCur().anData["animeName"]))
        else:
            output_list.append(playword + ": \n%s" % self.getCur().name)

        output_list.append("Time: %s\\%s" % (self.timer.getTimeFor(), EveconLib.Tools.TimeFor(self.getCur().pygletData.duration)))
        if self.muted:
            # output.append(int((console_data["pixx"]/2)-3)*"|"+"Muted"+int((console_data["pixx"]/2)-2)*"|")
            l1 = ""
            l2 = ""
            pre = ""
            output_list.append(
                pre + int((EveconLib.Config.console_data["pixx"] / 2) - 3) * l1 + "Muted" + int((EveconLib.Config.console_data["pixx"] / 2) - 2) * l2)
        if self.exitn:
            output_list.append("Will exit after this track!")

        return output_list

    def return_head_noti(self):
        # notification

        output_list = []

        old_noti = self.notifications.copy()
        del_msg = []
        del_not = []
        title_used = False

        def workAmsg(msgID):
            nonlocal title_used
            if old_noti[notiID]["msgs"][msgID]["starttime"] \
                    + old_noti[notiID]["msgs"][msgID]["screentime"] < time.time():  # invalid msg time
                del_msg.append((notiID, msgID))
            else:
                if old_noti[notiID]["title"] and not title_used:
                    title_used = True
                    output_list.append(old_noti[notiID]["title"] + ":")
                output_list.append(old_noti[notiID]["msgs"][msgID]["msg"])

        def workANote(noti_id):
            nonlocal title_used
            if old_noti[noti_id]["maxTime"]:
                if time.time() > old_noti[noti_id]["maxTime"] + old_noti[noti_id]["maxTimestart"]:  # invalid: MAXTIME
                    del_not.append(noti_id)
            else:
                title_used = False
                # if len(old_noti[noti_id]["msgs"]) == 1:
                #    workAmsg(0)
                for msgID in range(0, len(old_noti[noti_id]["msgs"])):
                    workAmsg(msgID)

                if len(old_noti[noti_id]["msgs"]) == 0:
                    del_not.append(noti_id)

        # if len(old_noti) == 0:
        #    workANote(0)
        for notiID in range(0, len(old_noti)):
            workANote(notiID)

        for x in range(-1, -len(del_msg) - 1, -1):
            del self.notifications[del_msg[x][0]]["msgs"][del_msg[x][1]]
        for x in range(-1, -len(del_not) - 1, -1):
            del self.notifications[del_not[x]]

        return output_list

    def print_head_info(self, forcePrint=False):
        if self.neverPrint and not forcePrint:
            return False
        for line in self.return_head_info():
            print(line)

    def print_head_noti(self, forcePrint=False):
        if self.neverPrint and not forcePrint:
            return False
        for line in self.return_head_noti():
            print(line)

    def print_head(self, forcePrint=False):
        # Info-Container

        self.print_head_info(forcePrint)

        if self.return_head_noti() and not self.neverPrint:
            print("\n" + EveconLib.Config.console_data["pixx"] * "-" + "\n")
            self.print_head_noti(forcePrint)

        # sys.stdout.write(console_data[0]*"-")

    def return_body(self):
        output_list = []

        # Main-Container

        if self.con_main == "details":
            output_list.append("Details:\n")
            output_list.append("Duration: " + str(EveconLib.Tools.TimeFor(self.playlist[self.cur_Pos].pygletData.duration)))

            if self.playlist[self.cur_Pos].anData["valid"]:
                output_list.append("Title: " + str(self.playlist[self.cur_Pos].anData["title"]))
                output_list.append(
                    "Interpreter: " + str(self.playlist[self.cur_Pos].anData["interpreter"]))
                output_list.append("Musictype: " + str(self.playlist[self.cur_Pos].anData["musictype"]))
                output_list.append("Animename: " + str(self.playlist[self.cur_Pos].anData["animeName"]))
                if self.playlist[self.cur_Pos].anData.get("animeSeason"):
                    output_list.append("Season: " + str(self.playlist[self.cur_Pos].anData["animeSeason"]))
                if self.playlist[self.cur_Pos].anData.get("animeType"):
                    output_list.append("Type: " + str(self.playlist[self.cur_Pos].anData["animeType"])
                                       + str(self.playlist[self.cur_Pos].anData["animeTypeNum"]))

            output_list.append("Filename: " + self.playlist[self.cur_Pos].file)
            output_list.append("Musictype: " + self.playlist[self.cur_Pos].type)
            output_list.append("Parantkey: " + str(self.playlist[self.cur_Pos].parentKey))
            output_list.append("Filepath: " + self.playlist[self.cur_Pos].path)
            output_list.append("Last file change: " + str(self.playlist[self.cur_Pos].time))
            if self.playlist[self.cur_Pos].subAvail:
                output_list.append("Subtitlepath: " + self.playlist[self.cur_Pos].subFile)
            output_list.append("Filepath: " + self.playlist[self.cur_Pos].path)
            output_list.append("Album: " + self.playlist[self.cur_Pos].pygletData.info.album.decode())
            output_list.append("Author: " + self.playlist[self.cur_Pos].pygletData.info.author.decode())
            output_list.append("Comment: " + self.playlist[self.cur_Pos].pygletData.info.comment.decode())
            output_list.append("Copyright: " + self.playlist[self.cur_Pos].pygletData.info.copyright.decode())
            output_list.append("Genre: " + self.playlist[self.cur_Pos].pygletData.info.genre.decode())
            output_list.append("Title: " + self.playlist[self.cur_Pos].pygletData.info.title.decode())
            output_list.append("Track: " + str(self.playlist[self.cur_Pos].pygletData.info.track))
            output_list.append("Year: " + str(self.playlist[self.cur_Pos].pygletData.info.year))

        elif self.con_main == "info":
            output_list.append("Infos:\n")

            if self.server:
                output_list.append("Server: Alive %s, Port %s" % (str(self.server.is_alive()), str(self.server.port)))
            else:
                output_list.append("Server: Alive False, Port None")

            if self.remoteAddress:
                output_list.append("Remote address: " + self.remoteAddress)
                output_list.append("Remote action: " + self.remoteAction)

            output_list.append("\nPlayer:\n")

            output_list.append("Vol: " + str(EveconLib.Tools.Windows.Volume.getVolume()))
            output_list.append("Volplayer: " + str(self.volumep))
            output_list.append("Muted: " + str(self.muted))
            output_list.append("Playing: " + str(self.playing))
            output_list.append("Paused: " + str(self.paused))

            files_loaded_keys = ""
            for k in self.mfl.files_loadedKeys:
                files_loaded_keys += str(k) + ", "
            files_loaded_keys.rstrip(", ")
            output_list.append("Loaded-Key: " + files_loaded_keys)

            if self.debug:
                output_list.append("\nDebugging Details:\n")

                output_list.append("Cur-Pos: " + str(self.cur_Pos))
                output_list.append("Autorefresh: " + str(self.autorefresh))

                output_list.append("\nOther:\n")

                output_list.append("Scanner-Status: " + str(self.scanner.is_alive()))
                output_list.append("Timer direct: " + str(self.timer.getTime()))  # playing time
                output_list.append("Last print: " + str(self.last_print))
                output_list.append("Last auto print: " + str(self.last_print_auto))

        elif self.con_main == "spl":
            output_list += self.spl.returnmain()
            # self.spl.printit(False)

        elif self.con_main == "rhy":
            output_list += self.rhy.getPrint()

        elif self.con_main == "sub":
            if self.sub:
                output_list.append(self.sub.getSub(self.timer.getTime()))
            else:
                output_list.append("No sub available")
        # elif self.con_main == "edit":
        #     output_list.append("Edit the mainList: (%s)\n" % str(len(self.mainList.ge)))
        #
        #     search_done = False
        #     for now in range(self.expandRange):
        #         if not search_done:
        #             if self.cur_Pos == now:
        #                 if self.expandRange >= len(self.mainList) - 1:
        #                     for word_num in range(0, len(self.mainList)):
        #                         if word_num + 1 < 10:
        #                             word_num_str = str(word_num + 1) + "  "
        #                         elif word_num + 1 < 100:
        #                             word_num_str = str(word_num + 1) + " "
        #                         else:
        #                             word_num_str = str(word_num + 1)
        #
        #                         if self.cur_Pos == word_num:
        #                             if not self.debug:
        #                                 if len(self.mainList[word_num].name) > 108:
        #                                     output_list.append(" " + word_num_str + " * " + EveconLib.Tools.getPartStr(
        #                                         self.mainList[word_num].name, 0, 108) + "...")
        #                                 else:
        #                                     output_list.append(
        #                                         " " + word_num_str + " * " + self.mainList[word_num].name)
        #                             else:
        #                                 output_list.append(
        #                                     " " + word_num_str + " * " + self.mainList[word_num].name + "0" + self.mainList[word_num])
        #                         else:
        #                             if not self.debug:
        #                                 if len(self.mainList[word_num].name) > 108:
        #                                     output_list.append(" " + word_num_str + "   " + EveconLib.Tools.getPartStr(
        #                                         self.mainList[word_num].name, 0, 108) + "...")
        #                                 else:
        #                                     output_list.append(
        #                                         " " + word_num_str + "   " + self.mainList[word_num].name)
        #                             else:
        #                                 output_list.append(
        #                                     " " + word_num_str + "   " + self.mainList[word_num].name + "1" + self.mainList[word_num])
        #                 elif 2 * self.expandRange + 1 >= len(self.mainList):
        #                     for word_num in range(0, 2 * self.expandRange + 1):  # + 1?
        #                         if word_num + 1 < 10:
        #                             word_num_str = str(word_num + 1) + "  "
        #                         elif word_num + 1 < 100:
        #                             word_num_str = str(word_num + 1) + " "
        #                         else:
        #                             word_num_str = str(word_num + 1)
        #
        #                         if self.cur_Pos == word_num:
        #                             try:
        #                                 if not self.debug:
        #                                     if len(self.mainList[word_num].name) > 108:
        #                                         output_list.append(" " + word_num_str + " * " + EveconLib.Tools.getPartStr(
        #                                             self.mainList[word_num].name, 0, 108) + "...")
        #                                     else:
        #                                         output_list.append(
        #                                             " " + word_num_str + " * " + self.mainList[word_num].name)
        #                                 else:
        #                                     output_list.append(
        #                                         " " + word_num_str + " * " + self.mainList[word_num].name + "2" + self.mainList[word_num])
        #                             except IndexError:
        #                                 pass
        #                         else:
        #                             try:
        #                                 if not self.debug:
        #                                     if len(self.mainList[word_num].name) > 108:
        #                                         output_list.append(" " + word_num_str + "   " + EveconLib.Tools.getPartStr(
        #                                             self.mainList[word_num].name, 0, 108) + "...")
        #                                     else:
        #                                         output_list.append(
        #                                             " " + word_num_str + "   " + self.mainList[word_num].name)
        #                                 else:
        #                                     output_list.append(
        #                                         " " + word_num_str + "   " + self.mainList[word_num].name + "3" + self.mainList[word_num])
        #                             except IndexError:
        #                                 pass
        #                 else:
        #                     for word_num in range(0, 2 * self.expandRange + 1):  # + 1? # begin
        #                         if word_num + 1 < 10:
        #                             word_num_str = str(word_num + 1) + "  "
        #                         elif word_num + 1 < 100:
        #                             word_num_str = str(word_num + 1) + " "
        #                         else:
        #                             word_num_str = str(word_num + 1)
        #                         if self.cur_Pos == word_num:
        #                             if not self.debug:
        #                                 if len(self.mainList[word_num].name) > 108:
        #                                     output_list.append(" " + word_num_str + " * " + EveconLib.Tools.getPartStr(
        #                                         self.mainList[word_num].name, 0, 108) + "...")
        #                                 else:
        #                                     output_list.append(
        #                                         " " + word_num_str + " * " + self.mainList[word_num].name)
        #                             else:
        #                                 output_list.append(
        #                                     " " + word_num_str + " * " + self.mainList[word_num].name + "4" + self.mainList[word_num])
        #                         else:
        #                             if not self.debug:
        #                                 if len(self.mainList[word_num].name) > 108:
        #                                     output_list.append(" " + word_num_str + "   " + EveconLib.Tools.getPartStr(
        #                                         self.mainList[word_num].name, 0, 108) + "...")
        #                                 else:
        #                                     output_list.append(
        #                                         " " + word_num_str + "   " + self.mainList[word_num].name)
        #                             else:
        #                                 output_list.append(
        #                                     " " + word_num_str + "   " + self.mainList[word_num].name + "5" + self.mainList[word_num])
        #                 search_done = True
        #                 break
        #
        #             elif self.cur_Pos == len(self.mainList) - now - 1 and self.cur_Pos >= self.expandRange:  # Ende
        #                 for word_num in range(self.cur_Pos - self.expandRange - 2 + now, self.cur_Pos + 1 + now):
        #                     if word_num < 0:
        #                         continue
        #                     # outputList.append(word_num, self.curPos, now, self.expandRange)
        #                     if word_num + 1 < 10:
        #                         word_num_str = str(word_num + 1) + "  "
        #                     elif word_num + 1 < 100:
        #                         word_num_str = str(word_num + 1) + " "
        #                     else:
        #                         word_num_str = str(word_num + 1)
        #                     if self.cur_Pos == word_num:
        #                         if not self.debug:
        #                             if len(self.mainList[word_num].name) > 108:
        #                                 output_list.append(" " + word_num_str + " * " + EveconLib.Tools.getPartStr(
        #                                     self.mainList[word_num].name, 0, 108) + "...")
        #                             else:
        #                                 output_list.append(
        #                                     " " + word_num_str + " * " + self.mainList[word_num].name)
        #                         else:
        #                             output_list.append(
        #                                 " " + word_num_str + " * " + self.mainList[word_num].name + "6" + self.mainList[word_num])
        #                     else:
        #                         if not self.debug:
        #                             if len(self.mainList[word_num].name) > 108:
        #                                 output_list.append(" " + word_num_str + "   " + EveconLib.Tools.getPartStr(
        #                                     self.mainList[word_num].name, 0, 108) + "...")
        #                             else:
        #                                 output_list.append(
        #                                     " " + word_num_str + "   " + self.mainList[word_num].name)
        #                         else:
        #                             output_list.append(
        #                                 " " + word_num_str + "   " + self.mainList[word_num].name + "7" + self.mainList[word_num])
        #                 search_done = True
        #                 break
        #
        #     if not search_done:  # mid
        #         for word_num in range(self.cur_Pos - self.expandRange, self.cur_Pos + self.expandRange + 1):
        #             if word_num + 1 < 10:
        #                 word_num_str = str(word_num + 1) + "  "
        #             elif word_num + 1 < 100:
        #                 word_num_str = str(word_num + 1) + " "
        #             else:
        #                 word_num_str = str(word_num + 1)
        #             if self.cur_Pos == word_num:
        #                 if not self.debug:
        #                     if len(self.mainList[word_num].name) > 108:
        #                         output_list.append(
        #                             " " + word_num_str + " * " + EveconLib.Tools.getPartStr(
        #                                 self.mainList[word_num].name,
        #                                 0, 108) + "...")
        #                     else:
        #                         output_list.append(
        #                             " " + word_num_str + " * " + self.mainList[word_num].name)
        #                 else:
        #                     output_list.append(
        #                         " " + word_num_str + " * " + self.mainList[word_num].name + "10" +
        #                         self.mainList[word_num])
        #             else:
        #                 if not self.debug:
        #                     if len(self.mainList[word_num].name) > 108:
        #                         output_list.append(
        #                             " " + word_num_str + "   " + EveconLib.Tools.getPartStr(
        #                                 self.mainList[word_num].name,
        #                                 0, 108) + "...")
        #                     else:
        #                         output_list.append(
        #                             " " + word_num_str + "   " + self.mainList[word_num].name)
        #                 else:
        #                     output_list.append(
        #                         " " + word_num_str + "   " + self.mainList[word_num].name + "11" +
        #                         self.mainList[word_num])
        else:
            if self.con_main == "pl":
                output_list.append("Playlist: (%s)\n" % str(len(self.playlist)))  # header
            elif self.con_main == "edit":
                output_list.append("Editlist: (%s)\n" % str(len(self.mainList.getCurList())))  # header
            elif self.con_main == "show":
                output_list.append("Showlist: (%s)\n" % str(len(self.mainList.getCurList())))  # header
            else:
                output_list.append("Content main cannot show '%s', showing current list" % self.con_main)

            search_done = False
            for now in range(self.expandRange):
                if not search_done:
                    if self.cur_Pos == now:
                        if self.expandRange >= len(self.mainList.getCurList()) - 1:
                            for word_num in range(0, len(self.mainList.getCurList())):
                                if word_num + 1 < 10:
                                    word_num_str = str(word_num + 1) + "  "
                                elif word_num + 1 < 100:
                                    word_num_str = str(word_num + 1) + " "
                                else:
                                    word_num_str = str(word_num + 1)

                                if self.cur_Pos == word_num:
                                    if not self.debug:
                                        if len(self.mainList.getCurList()[word_num].name) > 108:
                                            output_list.append(" " + word_num_str + " * " + EveconLib.Tools.getPartStr(
                                                self.mainList.getCurList()[word_num].name, 0, 108) + "...")
                                        else:
                                            output_list.append(
                                                " " + word_num_str + " * " + self.mainList.getCurList()[word_num].name)
                                    else:
                                        output_list.append(
                                            " " + word_num_str + " * " + self.mainList.getCurList()[word_num].name + "0" +
                                            str(self.mainList.getCurList()[word_num]))
                                else:
                                    if not self.debug:
                                        if len(self.mainList.getCurList()[word_num].name) > 108:
                                            output_list.append(" " + word_num_str + "   " + EveconLib.Tools.getPartStr(
                                                self.mainList.getCurList()[word_num].name, 0, 108) + "...")
                                        else:
                                            output_list.append(
                                                " " + word_num_str + "   " + self.mainList.getCurList()[word_num].name)
                                    else:
                                        output_list.append(
                                            " " + word_num_str + "   " + self.mainList.getCurList()[word_num].name + "1" +
                                            str(self.mainList.getCurList()[word_num]))
                        elif 2 * self.expandRange + 1 >= len(self.mainList.getCurList()):
                            for word_num in range(0, 2 * self.expandRange + 1):  # + 1?
                                if word_num + 1 < 10:
                                    word_num_str = str(word_num + 1) + "  "
                                elif word_num + 1 < 100:
                                    word_num_str = str(word_num + 1) + " "
                                else:
                                    word_num_str = str(word_num + 1)

                                if self.cur_Pos == word_num:
                                    try:
                                        if not self.debug:
                                            if len(self.mainList.getCurList()[word_num].name) > 108:
                                                output_list.append(" " + word_num_str + " * " + EveconLib.Tools.getPartStr(
                                                    self.mainList.getCurList()[word_num].name, 0, 108) + "...")
                                            else:
                                                output_list.append(
                                                    " " + word_num_str + " * " + self.mainList.getCurList()[word_num].name)
                                        else:
                                            output_list.append(
                                                " " + word_num_str + " * " + self.mainList.getCurList()[word_num].name
                                                + "2" + str(self.mainList.getCurList()[word_num]))
                                    except IndexError:
                                        pass
                                else:
                                    try:
                                        if not self.debug:
                                            if len(self.mainList.getCurList()[word_num].name) > 108:
                                                output_list.append(" " + word_num_str + "   " + EveconLib.Tools.getPartStr(
                                                    self.mainList.getCurList()[word_num].name, 0, 108) + "...")
                                            else:
                                                output_list.append(
                                                    " " + word_num_str + "   " + self.mainList.getCurList()[word_num].name)
                                        else:
                                            output_list.append(
                                                " " + word_num_str + "   " + self.mainList.getCurList()[word_num].name + "3" + str(self.playlist[word_num]))
                                    except IndexError:
                                        pass
                        else:
                            for word_num in range(0, 2 * self.expandRange + 1):  # + 1? # Anfang
                                if word_num + 1 < 10:
                                    word_num_str = str(word_num + 1) + "  "
                                elif word_num + 1 < 100:
                                    word_num_str = str(word_num + 1) + " "
                                else:
                                    word_num_str = str(word_num + 1)
                                if self.cur_Pos == word_num:
                                    if not self.debug:
                                        if len(self.mainList.getCurList()[word_num].name) > 108:
                                            output_list.append(" " + word_num_str + " * " + EveconLib.Tools.getPartStr(
                                                self.mainList.getCurList()[word_num].name, 0, 108) + "...")
                                        else:
                                            output_list.append(
                                                " " + word_num_str + " * " + self.mainList.getCurList()[word_num].name)
                                    else:
                                        output_list.append(
                                            " " + word_num_str + " * " + self.mainList.getCurList()[word_num].name + "4" +
                                            str(self.mainList.getCurList()[word_num]))
                                else:
                                    if not self.debug:
                                        if len(self.mainList.getCurList()[word_num].name) > 108:
                                            output_list.append(" " + word_num_str + "   " + EveconLib.Tools.getPartStr(
                                                self.mainList.getCurList()[word_num].name, 0, 108) + "...")
                                        else:
                                            output_list.append(
                                                " " + word_num_str + "   " + self.mainList.getCurList()[word_num].name)
                                    else:
                                        output_list.append(
                                            " " + word_num_str + "   " + self.mainList.getCurList()[word_num].name + "5" +
                                            str(self.mainList.getCurList()[word_num]))
                        search_done = True
                        break

                    elif self.cur_Pos == len(self.mainList.getCurList()) - now - 1 and self.cur_Pos >= self.expandRange:  # Ende
                        for word_num in range(self.cur_Pos - self.expandRange - 2 + now, self.cur_Pos + 1 + now):
                            if word_num < 0:
                                continue
                            # outputList.append(word_num, self.curPos, now, self.expandRange)
                            if word_num + 1 < 10:
                                word_num_str = str(word_num + 1) + "  "
                            elif word_num + 1 < 100:
                                word_num_str = str(word_num + 1) + " "
                            else:
                                word_num_str = str(word_num + 1)
                            if self.cur_Pos == word_num:
                                if not self.debug:
                                    if len(self.mainList.getCurList()[word_num].name) > 108:
                                        output_list.append(" " + word_num_str + " * " + EveconLib.Tools.getPartStr(
                                            self.mainList.getCurList()[word_num].name, 0, 108) + "...")
                                    else:
                                        output_list.append(
                                            " " + word_num_str + " * " + self.mainList.getCurList()[word_num].name)
                                else:
                                    output_list.append(
                                        " " + word_num_str + " * " + self.mainList.getCurList()[word_num].name + "6" +
                                        str(self.mainList.getCurList()[word_num]))
                            else:
                                if not self.debug:
                                    if len(self.mainList.getCurList()[word_num].name) > 108:
                                        output_list.append(" " + word_num_str + "   " + EveconLib.Tools.getPartStr(
                                            self.mainList.getCurList()[word_num].name, 0, 108) + "...")
                                    else:
                                        output_list.append(
                                            " " + word_num_str + "   " + self.mainList.getCurList()[word_num].name)
                                else:
                                    output_list.append(
                                        " " + word_num_str + "   " + self.mainList.getCurList()[word_num].name + "7" +
                                        str(self.mainList.getCurList()[word_num]))
                        search_done = True
                        break

            if not search_done:  # mid
                for word_num in range(self.cur_Pos - self.expandRange, self.cur_Pos + self.expandRange + 1):
                    if word_num + 1 < 10:
                        word_num_str = str(word_num + 1) + "  "
                    elif word_num + 1 < 100:
                        word_num_str = str(word_num + 1) + " "
                    else:
                        word_num_str = str(word_num + 1)
                    if self.cur_Pos == word_num:
                        if not self.debug:
                            if len(self.mainList.getCurList()[word_num].name) > 108:
                                output_list.append(
                                    " " + word_num_str + " * " + EveconLib.Tools.getPartStr(self.mainList.getCurList()[word_num].name,
                                                                                            0,
                                                                                            108) + "...")
                            else:
                                output_list.append(
                                    " " + word_num_str + " * " + self.mainList.getCurList()[word_num].name)
                        else:
                            output_list.append(
                                " " + word_num_str + " * " + self.mainList.getCurList()[word_num].name + "10" +
                                str(self.mainList.getCurList()[word_num]))
                    else:
                        if not self.debug:
                            if len(self.mainList.getCurList()[word_num].name) > 108:
                                output_list.append(
                                    " " + word_num_str + "   " + EveconLib.Tools.getPartStr(self.mainList.getCurList()[word_num].name,
                                                                                            0,
                                                                                            108) + "...")
                            else:
                                output_list.append(
                                    " " + word_num_str + "   " + self.mainList.getCurList()[word_num].name)
                        else:
                            output_list.append(
                                " " + word_num_str + "   " + self.mainList.getCurList()[word_num].name + "11" +
                                str(self.mainList.getCurList()[word_num]))

        return output_list

    def print_body(self, forcePrint=False):
        if self.neverPrint and not forcePrint:
            return False

        for line in self.return_body():
            print(line)

    def return_foot(self):
        output_list = []

        if self.con_cont == "set":
            output_list.append("Commands:\n")

            if not self.paused:
                output_list.append(
                    "Pause (P), Next (N), Reroll all (RE), Details (DEA)")
            elif self.paused:
                output_list.append(
                    "Play (P), Next (N), Reroll all (RE), Details (DEA)")

            if self.con_main == "spl":
                output_list += self.spl.returncom()
                # self.spl.printcom()

            output_list.append("\nInput:\n%s" % self.cur_Input)

        elif self.con_cont == "edit":
            output_list.append("Commands: (with UPPER LETTERs)\n")

            output_list.append("Play this (P), Delthis (DEL), Queue this (QU)")

            output_list.append("\nInput: %s" % self.cur_Input.upper())

            output_list.append("\nSearch: (with lower letters)\n")
            output_list.append("Input: %s" % self.cur_Search)

        elif self.con_cont == "conf":
            output_list.append("Confirm\n")
            output_list.append("Y/N")

        elif self.con_cont == "cont":
            output_list.append("Continue?\n")
            output_list.append("Press something")

        elif self.con_cont == "volp":
            output_list.append("Change Volume (Player):\n")
            output_list.append("Current: " + str(self.volumep))

            output_list.append("\n" + self.cur_Input)

        elif self.con_cont == "volw":
            self.volume = EveconLib.Tools.Windows.Volume.getVolume()
            output_list.append("Change Volume (Windows):\n")
            output_list.append("Current: " + str(self.volume))

            output_list.append("\n" + self.cur_Input)

        elif self.con_cont == "spe":
            output_list.append("Change Effectduration (Spl):\n")
            output_list.append("Current: " + str(self.spl.Effect))

            output_list.append("\n" + self.cur_Input)

        elif self.con_cont == "add":
            output_list.append("Add new Music:\n")

            cur = ""
            for mus in self.mfl.files_loadedKeys:
                cur += mus.key + ", "
            cur = cur.rstrip(", ")

            output_list.append("Current: " + cur)

            mis_str = ""
            for misPart in self.musicKeysLeft:
                mis_str += misPart + ", "
            mis_str = mis_str.rstrip(", ")

            output_list.append("Left: " + mis_str)

            output_list.append("\n" + self.cur_Input)

        elif self.con_cont == "dekey":
            output_list.append("Remove music through their key:\n")

            cur = ""
            for mus in self.mfl.files_loadedKeys:
                cur += mus.key + ", "
            cur = cur.rstrip(", ")

            output_list.append("Current: " + cur)

            output_list.append("\n" + self.cur_Input)

        return output_list

    def print_foot(self, forcePrint=False):
        if self.neverPrint and not forcePrint:
            return False
        for line in self.return_foot():
            print(line)

    def returnit(self):
        output_list = []

        # Head-Container (Info + Noti)
        output_list += self.return_head_info()

        if self.return_head_noti():
            output_list += ["\n" + EveconLib.Config.console_data["pixx"] * "-" + "\n"]
            output_list += self.return_head_noti()

        output_list += ["\n" + EveconLib.Config.console_data["pixx"] * "-" + "\n"]

        # Main-Container
        output_list += self.return_body()

        output_list += ["\n" + EveconLib.Config.console_data["pixx"] * "-" + "\n"]
        # Control-Container
        output_list += self.return_foot()

        return output_list

    def printit(self, forcePrint=False):
        if self.neverPrint and not forcePrint:
            return False

        EveconLib.Tools.cls()
        for line in self.returnit():
            print(line)
        self.last_print = time.time()

    def react(self, inp, debug_directMulti=False):
        if debug_directMulti:
            self.cur_Input += inp
            x = self.input(self.cur_Input)
            self.printit()
            return x

        if self.lastPresses[1] + 0.2 >= time.time() and self.lastPresses[2] == inp:  # DOUBLE PRESS
            multi_press = self.lastPresses[0] + 1
            self.lastPresses = [multi_press, time.time(), inp]
        else:
            multi_press = 1
            self.lastPresses = [multi_press, time.time(), inp]
        while self.starttime + 1.5 >= time.time() and self.hardworktime + 0.65 >= time.time():
            time.sleep(0.25)

        if self.con_main == "details" or self.con_main == "info" or self.con_cont == "cont":
            self.con_main = self.con_main_last
            self.con_cont = "set"

        # Search BLOCK

        elif inp == "escape" and self.editing and self.mainList.activeLayer == "edit":
            self.con_main = "pl"
            self.con_cont = "set"
            self.editing = False
            self.cur_Search = ""
            self.cur_Input = ""
            self.cur_Pos = 0
            self.mainList.switchToPlaylist()

        elif inp == "escape" and self.editing and self.mainList.activeLayer == "show":
            self.mainList.switchToEdit(remake=False)
            self.cur_Input = ""
            self.cur_Search = ""
            self.cur_Pos = 0
            self.con_main = "edit"

        elif inp == " " and self.editing:
            self.cur_Search += " "
            self.mainList.getCurList().search(self.cur_Search)
            self.cur_Pos = 0

        elif len(inp) == 1 and self.editing:
            if inp == inp.lower():  # SEARCH
                self.cur_Search += inp
                self.mainList.getCurList().search(self.cur_Search)
                self.cur_Pos = 0

            else:  # COMMANDS
                self.cur_Input += inp

                i = self.cur_Input.lower()

                if self.mainList.activeLayer == "edit":  # edit list commands
                    if i == "show":  # switch to show list
                        self.mainList.switchToShow()
                        self.cur_Input = ""
                        self.cur_Search = ""
                        self.cur_Pos = 0
                        self.con_main = "show"
                if self.mainList.activeLayer == "show":  # show list commands
                    if i == "edit":  # switch to show list
                        self.mainList.switchToEdit(remake=False)
                        self.cur_Input = ""
                        self.cur_Search = ""
                        self.cur_Pos = 0
                        self.con_main = "edit"

                    if i == "del":
                        self.mainList.getCurList().hideFile(self.idToF(self.cur_Pos))  # remove this item from this list too

                # global commands for both
                if i == "p":  # move this file to pos 0
                    self.mainList.editList.moveFileToPos(self.idToF(self.cur_Pos), 0)
                    self.cur_Input = ""

                elif i == "del":  # delete (hide) this file
                    self.mainList.editList.hideFile(self.idToF(self.cur_Pos))  # auto apply
                    self.cur_Input = ""

                elif i == "qu":  # move this file to pos 1, to be played after 0
                    file = self.idToF(self.cur_Pos)
                    # if file in self.mainList.editList.mainList:
                    self.mainList.editList.moveFileToPos(self.idToF(self.cur_Pos), 1)
                    # else:
                    #     self.mainList.editList.insert(1, file)
                    self.cur_Input = ""
                elif i == "re":
                    self.mainList.getCurList().shuffle()
                    self.cur_Input = ""

                # TODO: add commands: sort

                # Musicplayer commands
                if i == "pla" or i == "pau":
                    self.switch()
                    self.cur_Input = ""
                elif i == "next" or i == "n":
                    self.next()
                    self.cur_Input = ""
                elif i == "m":
                    self.switchmute()
                    self.cur_Input = ""
                elif i == "stop" or i == "exit":
                    self.stop()
                    self.cur_Input = ""

        elif inp == "strg_arrowup" and self.cur_Pos > 0 and self.editing or \
                inp == "num8" and self.cur_Pos > 0 and self.editing:

            self.mainList.getCurList().moveFileToPos(self.idToF(self.cur_Pos), self.cur_Pos-1)

            self.cur_Pos -= 1

        elif inp == "strg_arrowdown" and self.cur_Pos < len(self.mainList.getCurList()) - 1 and self.editing or \
                inp == "num2" and self.cur_Pos < len(self.mainList.getCurList()) - 1 and self.editing:

            self.mainList.getCurList().moveFileToPos(self.idToF(self.cur_Pos), self.cur_Pos+1)

            self.cur_Pos += 1

        elif inp == "del" and self.editing:  # delete key
            self.mainList.editList.hideFile(self.idToF(self.cur_Pos))  # auto apply

        elif inp == "return" and self.editing:  # apply the edit mainList
            self.mainList.applyToPlaylist()

            self.resetInterface()

        elif inp == "backspace" and self.editing:  # SEARCH part
            if len(self.cur_Search) > 0:
                self.cur_Search = self.cur_Search.rstrip(self.cur_Search[-1])
                self.mainList.getCurList().search(self.cur_Search)

        elif inp == "strg_backspace" and self.editing:  # COMMANDS part
            if len(self.cur_Input) > 0:
                new_input = ""
                for x in range(len(self.cur_Input) - 1):
                    new_input += self.cur_Input[x]
                self.cur_Input = new_input

        elif inp == "arrowup" and self.cur_Pos > 0 and self.editing:
            done = False
            for aS in self.arrowSetting:
                if aS[0] < multi_press < aS[1] != -1 and not done or aS[0] < multi_press and aS[1] == -1 and not done:
                    done = True
                    if self.cur_Pos - aS[2] >= 0:
                        self.cur_Pos -= aS[2]
                    else:
                        self.cur_Pos = 0
                    break

            if not done:
                self.cur_Pos -= 1

        elif inp == "arrowdown" and self.cur_Pos < len(self.mainList.getCurList()) - 1 and self.editing:
            done = False
            for aS in self.arrowSetting:
                if aS[0] < multi_press < aS[1] != -1 and not done or aS[0] < multi_press and aS[1] == -1 and not done:
                    done = True
                    if self.cur_Pos + aS[2] <= len(self.mainList.getCurList()) - 1:
                        self.cur_Pos += aS[2]
                    else:
                        self.cur_Pos = len(self.mainList.getCurList()) - 1
                    break
            if not done:
                self.cur_Pos += 1

        elif inp == " ":
            self.switch()

        elif len(inp) == 1 and not self.editing:  # MAIN give to next method
            self.cur_Input += inp
            if not self.change:
                if self.input(self.cur_Input):
                    self.cur_Input = ""

        elif inp == "backspace" and not self.editing:
            if len(self.cur_Input) > 0:
                new_input = ""
                for x in range(len(self.cur_Input) - 1):
                    new_input += self.cur_Input[x]
                self.cur_Input = new_input

        elif inp == "strg_backspace" and not self.editing:
            self.cur_Input = ""

        elif self.con_main != "pl" and inp == "escape":  # !! EXIT IN EVERYTHING WITH ESC
            self.resetInterface()

        elif inp == "return" and not self.editing:  # play this title
            self.mainList.pl_moveToZero(self.playlist[self.cur_Pos])

        elif self.change and inp == "escape":
            self.cur_Input = ""
            self.change = ""
            self.con_cont = "set"

        elif self.change == "volp":
            if inp == "return":
                try:
                    self.volp(float(self.cur_Input))
                except ValueError:
                    pass

                self.cur_Input = ""
                self.change = ""
                self.con_cont = "set"

        elif self.change == "volw":
            if inp == "return":
                try:
                    self.vol(float(self.cur_Input))
                except ValueError:
                    pass

                self.cur_Input = ""
                self.change = ""
                self.con_cont = "set"

        elif self.change == "spe":
            if inp == "return":
                try:
                    self.spl.ChEffect(int(self.cur_Input))
                except ValueError:
                    pass

                self.cur_Input = ""
                self.change = ""
                self.con_cont = "set"

        elif self.change == "add":
            if inp == "return":
                the_key = self.cur_Input

                def addMe():
                    if self.addMusic(the_key, printStaMSG=False, printEndMSG=False, make_notification=True):
                        self.make_playlist()

                t = threading.Thread(target=addMe)
                t.start()
                self.cur_Input = ""
                self.change = ""
                self.con_cont = "set"

        elif self.change == "dekey":
            if inp == "return":
                self.DelByKey(self.cur_Input)

                self.cur_Input = ""
                self.change = ""
                self.con_cont = "set"

        elif inp == "arrowup" and self.cur_Pos > 0 and self.con_main == "pl":
            done = False
            for aS in self.arrowSetting:
                if aS[0] < multi_press < aS[1] != -1 and not done or aS[0] < multi_press and aS[1] == -1 and not done:
                    done = True
                    if self.cur_Pos - aS[2] >= 0:
                        self.cur_Pos -= aS[2]
                    else:
                        self.cur_Pos = 0
                    break

            if not done:
                self.cur_Pos -= 1

        elif inp == "arrowdown" and self.cur_Pos < len(self.playlist) - 1 and self.con_main == "pl":
            done = False
            for aS in self.arrowSetting:
                if aS[0] < multi_press < aS[1] != -1 and not done or aS[0] < multi_press and aS[1] == -1 and not done:
                    done = True
                    if self.cur_Pos + aS[2] <= len(self.playlist) - 1:
                        self.cur_Pos += aS[2]
                    else:
                        self.cur_Pos = len(self.playlist) - 1
                    break
            if not done:
                self.cur_Pos += 1

        elif inp == "del" and self.con_main == "pl":
            self.idToF(self.cur_Pos).active = False
            if self.cur_Pos == 0:
                self.next(True)
            # self.playing = False
            # if self.paused:
            #     self.play()

        else:
            return False

        self.printit()

    def input(self, i):
        if i == i.upper():
            upper = True
        else:
            upper = False

        i = i.lower()
        if self.waitForVPstart:
            self.notify("WAIT FOR VIDEO START", "Error")
            # input()

            # some options for debug
            if i == "next" or i == "n":  # to exit from the video if something crashes
                self.next(forceKillVideo=True)

            self.cur_Input = ""
            return

        if i == "play" or i == "pau" or i == "pause" or i == "p":
            self.switch()
        elif i == "next" or i == "n":
            self.next()
        elif i == "m":
            self.switchmute()
        elif i == "stop" or i == "exit":
            self.stop()

        elif i == "add":
            self.cur_Input = ""
            self.change = "add"
            self.con_cont = "add"

            # generating the missing MusicKeysList
            self.musicKeysLeft = self.mfl.musicFileEditor.musicDirs["keys"].copy()
            for needToDel in self.mfl.files_loadedKeys:
                sol = EveconLib.Tools.Search(needToDel.key, self.musicKeysLeft, exact=True, lower=False)
                if len(sol) == 0:
                    continue  # maybe a mpl key
                del self.musicKeysLeft[sol[0]]

        elif i == "dekey":
            self.cur_Input = ""
            self.change = "dekey"
            self.con_cont = "dekey"
        elif i == "volw":
            self.cur_Input = ""
            self.change = "volw"
            self.con_cont = "volw"
        elif i == "volp":
            self.cur_Input = ""
            self.change = "volp"
            self.con_cont = "volp"
        elif i == "re":
            if not upper:
                self.mainList.pl_shuffle()
            else:
                self.restoreLastList()
        elif i == "lpl":
            self.restoreLastList()
        elif i == "exin":
            self.exitn = True
        elif i == "dea" and (self.con_main == "pl" or self.con_main == "edit"):
            self.con_main_last = self.con_main
            self.con_main = "details"
            self.con_cont = "cont"
        elif i == "info" and (self.con_main == "pl" or self.con_main == "edit"):
            self.con_main_last = self.con_main
            self.con_main = "info"
            self.con_cont = "cont"
        elif i == "debug":
            if self.debug:
                self.debug = False
            else:
                self.debug = True
        elif i == "refresh":
            self.refreshTitle()
            self.printit()

        elif i == "autorefresh" and self.debug:
            if self.autorefresh:
                self.autorefresh = False
            else:
                self.autorefresh = True

        elif i == "edit" and self.con_main == "pl":
            self.startEditing()

        elif i == "sub":
            if self.con_main == "sub":
                self.con_main = self.con_main_last
            else:
                self.con_main_last = self.con_main
                self.con_main = "sub"

        elif i == "vid":  # deactivate video for this session and play normal
            if self.cur_Pos == 0:
                if self.getCur().type == "music":
                    return False

                self.getCur().type = "music"
                self.stopVideoPlayer(debug_vPIP=True)

                time.sleep(3)
                self.player.pause()
                self.timer.reset()

                try:
                    self.getCur().pygletData._is_queued
                except AttributeError:
                    pass
                else:
                    self.getCur().loadForPyglet()

                self.player.next_source()
                self.player.queue(self.getCur().pygletData)
                self.player.play()
                self.timer.start()
                time.sleep(1.5)

                self.videoPlayerIsPlaying = False
            else:
                if self.playlist[self.cur_Pos].type == "music" and self.playlist[self.cur_Pos].videoAvail:
                    self.playlist[self.cur_Pos].type = "video"
                else:
                    self.playlist[self.cur_Pos].type = "music"

        elif i == "spl":
            if self.con_main == "spl":
                self.con_main_last = self.con_main
                self.con_main = "pl"
            else:
                self.con_main_last = self.con_main
                self.con_main = "spl"

        elif EveconLib.Tools.lsame(i, "sp") and not i == "sp" and self.con_main == "spl":
            if EveconLib.Tools.lsame(i, "spe"):
                self.cur_Input = ""
                self.change = "spe"
                self.con_cont = "spe"
            elif i == "spn":
                self.spl.RoundOverF()
            elif i == "spwr":
                self.spl.WRswitch()
            elif i == "spr":
                self.spl.WRreroll()
            else:
                return False

        elif i == "rhy":
            if self.con_main == "rhy":
                self.con_main_last = self.con_main
                self.con_main = "pl"
            else:
                self.rhy.start()
                self.con_main_last = self.con_main
                self.con_main = "rhy"

        elif self.con_main == "rhy":
            self.rhy.newPress()

        else:
            return False
        return True

    def react_remote(self, i, con_id=0, java=False):
        if con_id:  # needed to suppress the IDE msg => ignore this
            pass

        if isinstance(i, tuple):
            self.remoteAddress = i[0]
        elif i is None:
            self.remoteAddress = ""
            self.remoteAction = ""
        else:  # COMMANDS
            """
            HOW:

            type_what(_value)

            eg.: 
            set_pause_0
            get_pause
            """
            data = i.split("_")

            if data[0] == "set":
                if data[1] == "next":
                    self.next()
                    self.remoteAction = "Next"
                elif data[1] == "pause":
                    if len(data) == 3:
                        if data[2] == "0":
                            self.play()
                            self.remoteAction = "Unpause"
                        elif data[2] == "1":
                            self.pause()
                            self.remoteAction = "Pause"
                    else:
                        self.switch()
                        self.remoteAction = "Switch pause"
                elif data[1] == "mute":
                    if len(data) == 3:
                        if data[2] == "0":
                            self.unmute()
                            self.remoteAction = "Unmute"
                        elif data[2] == "1":
                            self.mute()
                            self.remoteAction = "Mute"
                    else:
                        self.switchmute()
                        self.remoteAction = "Switch mute"
                elif data[1] == "exit":
                    self.stop()
                elif data[1] == "volume" and len(data) == 3:
                    self.vol(float(data[2]))
                    self.remoteAction = "Volume to: " + str(self.volume)
                elif data[1] == "volumep" and len(data) == 3:
                    self.volp(float(data[2]))
                    self.remoteAction = "VolumeP to: " + str(self.volumep)

            elif data[0] == "get":
                data_send = ""
                if data[1] == "title":
                    data_send = self.getCur().name
                elif data[1] == "time":
                    data_send = round(self.timer.getTime())
                elif data[1] == "duration":
                    data_send = round(self.getCur().pygletData.duration)
                elif data[1] == "volume":
                    data_send = str(EveconLib.Tools.Windows.Volume.getVolume())
                # print(data_send)
                if data_send:
                    if not java:
                        self.server.send(data_send)

    def callVideoPlayer(self):
        # starts a videoPlayer
        self.stopVideoPlayer()

        self.waitForVPstart = True

        self.player.pause()
        new_port = EveconLib.Tools.UsedPorts.givePort()
        self.videoPlayerClient = EveconLib.Networking.Client(socket.gethostbyname(socket.gethostname()), new_port,
                                                             react=self.reactVideoPlayer, waitForConnection=True,
                                                             description="for VP from MP")
        sp = EveconLib.Config.startProgram
        cwd = sp.rstrip(sp.split(EveconLib.Config.path_seg)[-1]).rstrip(EveconLib.Config.path_seg)
        self.videoPlayerProcess = subprocess.Popen(["python", sp, "--vp", self.getCur().path, str(new_port)], cwd=cwd)

        time.sleep(5)

        self.videoPlayerClient.start()

        while self.videoPlayerClient.waitForConnection:  # catches all thread which want to send msg while client try to connect
            time.sleep(1)

        time.sleep(1)
        if self.videoPlayerClient.status == 2:
            self.videoPlayerClient.send("vol_"+str(self.volumep))
            self.videoPlayerIsPlaying = True
        else:
            EveconLib.Tools.Log("MusicPlayer", "Tried to start video player: Something crashed")
            self.notify("The video is corrupted", "ERROR")
            self.getCur().type = "music"
            self.replay(force_kill_video=True)

    def stopVideoPlayer(self, from_server=False, debug_vPIP=False, force=False):  # debug_vPIP do not chance vpip in this method
        """
        stops the Videoplayer

        :param from_server: is this method called from the vp_server
        :param debug_vPIP: ?
        :param force: force closing
        """
        if not self.videoPlayerIsPlaying and not force:
            return
        if not from_server:
            pass  # self.videoPlayerClient.send("exit")  # maybe kill this programm not through exit, because this could throw a error
        self.videoPlayerClient.exit(sendM=False)  # why not sendM if send from server?

        if not debug_vPIP:
            self.videoPlayerIsPlaying = False
            self.waitForVPstart = False  # maybe debug

        self.videoPlayerProcess.kill()

    def reactVideoPlayer(self, msg):
        if msg == "firstStart":
            self.waitForVPstart = False
        elif msg == "play":
            self.play(fromServer=True)
        elif msg == "pause":
            self.pause(fromServer=True)
        elif msg == "exit":
            self.next(fromServer=True)  # stop this video skip to next track
        elif EveconLib.Tools.lsame(msg, "vol_"):
            try:
                self.volp(float(msg.lstrip("vol_")), fromServer=True)
            except ValueError:
                pass
        elif msg == "mute":
            self.mute(fromServer=True)
        elif msg == "unmute":
            self.unmute(fromServer=True)
        else:
            return
        self.refresh(title=False, print_me=self.selfprint)

    def replay(self, force_kill_video=False):
        # should be a method to replay the current (id: 0) file
        self.next(skipthis=True, forceKillVideo=force_kill_video)

    def startEditing(self):
        self.con_main = "edit"
        self.con_cont = "edit"
        self.editing = True
        self.cur_Search = ""
        self.cur_Input = ""  # need this ?
        self.mainList.switchToEdit()
