"""
main of Flapi

the main class from which Nhee and Foxi and following Services will use

supports:
    - specified file
    - more modern file:
        - comments
        - possibility to add collections after the given page

    - clipboard management: instead if opening the files in the browser the link will be copied in the clipboard
    - automatic windscribe start, maybe changeable through changing the exe (config or param?)


"""